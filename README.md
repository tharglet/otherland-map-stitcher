# Otherland map stitcher
Stitches together map tiles from Otherland. Code assumes the file structure as it is when exporting from UModel with one quirk - some upks had multiple maps, so it will process maps in subfolders inside of Texture2D as I had to manually create folders for them. 

## Installing and running
1. Install Python 3, adding it to your PATH (Installer should do this for you)
1. Clone/download this repository
1. Open a terminal window
1. cd to where you put the project files
1. Run 'pip install -r requirements.txt' (seasoned Python runners can create a venv if they want :))
1. Run 'python stitch.py'
1. Put in the full path to the UModelExport folder, including itself
1. Wait
1. Wait more
1. Enjoy stitched maps!

