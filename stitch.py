''' Stitches Otherland's map tiles together, as exported by UModel '''
import os
from PIL import Image

def process_map(folder_name, pm_output_file):
    ''' Processes images in the given folder, puts into the given file (no extension needed)
    Parameters:
       folder_name: Folder containing the Tile images
       pm_output_file: Full file path of where to put file'''
    forage_x = 0
    forage_y = 0
    #Find map dimensions
    while os.path.isfile(f'{folder_name}\\Tile_{forage_x}_{0}.png'):
        forage_x += 1
    max_x = forage_x
    while os.path.isfile(f'{folder_name}\\Tile_{0}_{forage_y}.png'):
        forage_y += 1
    max_y = forage_y
    #Stitch!
    out_map = Image.new('RGB', (max_x * 512, max_y * 512), (255, 0, 0))
    for x in range (0, max_x):
        for y in range (0, max_y):
            tile = Image.open(f'{folder_name}\\Tile_{x}_{y}.png')
            out_map.paste(tile, (x * 512, y * 512))
    out_map.save(f'{pm_output_file}.png')

# Main
folder = input("Enter the folder to find umodel exports: ")
if os.path.isdir(folder):
    floor_map_list = os.listdir(folder)
    for floor_map_upper_folder in floor_map_list:
        if os.path.isdir(f'{folder}\\{floor_map_upper_folder}'):
            if floor_map_upper_folder.endswith('FloorMaps'):
                floor_map_inner = f'{folder}\\{floor_map_upper_folder}\\Texture2D'
                #If there is a map inside is folder, stitch it
                if os.path.isfile(f'{floor_map_inner}\\Tile_0_0.png'):
                    output_file_name = floor_map_upper_folder.split('_')[0]
                    output_file = f'{folder}\\{output_file_name}'
                    process_map(floor_map_inner, output_file)
                    print(f'Exported {output_file_name}')
                else:
                    print(f'Processing subfolders in {floor_map_upper_folder}')
                    #Walk folders - for when there was 'tile collisions' that necessitated
                    #manually moving tilesets to subfolders
                    for map_subfolder in os.listdir(floor_map_inner):
                        map_path = f'{floor_map_inner}\\{map_subfolder}'
                        if os.path.isdir(map_path):
                            main_file_name = floor_map_upper_folder.split('_')[0]
                            output_file_name = f'{main_file_name}-{map_subfolder}'
                            output_file = f'{folder}\\{output_file_name}'
                            process_map(map_path, output_file)
                            print(f'Exported {output_file_name}')
            else:
                print(f'Skipping {floor_map_upper_folder}')
else:
    print('Invalid folder!')
